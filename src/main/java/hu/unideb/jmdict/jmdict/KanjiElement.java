package hu.unideb.jmdict.jmdict;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import hu.unideb.jmdict.jmdict.interfaces.Extractable;
import hu.unideb.jmdict.jmdict.interfaces.HasChildren;
import hu.unideb.jmdict.jmdict.interfaces.HasParent;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * This class references k_ele from the JMDict.
 * 
 * @author Levente Daradics
 *
 */
@Entity
@Getter @Setter @NoArgsConstructor @ToString
public class KanjiElement implements HasParent, HasChildren, Extractable {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "entry_id")
	private Entry entry;
	
	/*
	 * This field references the keb field in JMDict.
	 */
	@Column(columnDefinition="TEXT") //for postgres
	private String kanjiElement;

	@Override
	public void setParent(HasChildren parent) {
		
		if(  !(parent instanceof Entry) )	{
			
			throw new IllegalArgumentException("Input must be type of Entry");
		}
		
		setEntry( (Entry)parent);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((kanjiElement == null) ? 0 : kanjiElement.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KanjiElement other = (KanjiElement) obj;
		if (kanjiElement == null) {
			if (other.kanjiElement != null)
				return false;
		} else if (!kanjiElement.equals(other.kanjiElement))
			return false;
		return true;
	}
	
	
	

}
