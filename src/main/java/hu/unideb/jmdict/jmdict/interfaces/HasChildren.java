package hu.unideb.jmdict.jmdict.interfaces;

import java.util.List;

public interface HasChildren {

	public default void setChildren(List<? extends HasParent> children)	{
		
		for(HasParent hp : children)	{
			
			hp.setParent(this);
		}
	}
	
	
	public default void setChild(HasParent child)	{
		
		child.setParent(this);
	}
}
