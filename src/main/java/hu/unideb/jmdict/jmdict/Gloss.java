package hu.unideb.jmdict.jmdict;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.neovisionaries.i18n.LanguageAlpha3Code;

import hu.unideb.jmdict.jmdict.interfaces.Extractable;
import hu.unideb.jmdict.jmdict.interfaces.HasChildren;
import hu.unideb.jmdict.jmdict.interfaces.HasParent;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @NoArgsConstructor @ToString
public class Gloss implements HasParent, HasChildren, Extractable{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	//@JoinColumn(name = "sense_id")
	private Sense sense;
	
	@Column(columnDefinition="TEXT") //for postgres
	private String meaningText;
	
	/*
	 *  This is an attribute in the JMDict gloss element. Marks the (non-Japanese) language 
	 */
	@Column
	private LanguageAlpha3Code languageCode;
	
	
	/*
	 * This is an attribute in the JMDict gloss element. 
	 */
	@Column
	private String gender;
	
	/*
	 * This is an attribute in the JMDict gloss element.
	 */
	@Column
	private String type;

	@Override
	public void setParent(HasChildren parent) {
		
		if(  !(parent instanceof Sense) )	{
			
			throw new IllegalArgumentException("Input must be type of Sense");
		}
		
		setSense( (Sense)  parent );
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((languageCode == null) ? 0 : languageCode.hashCode());
		result = prime * result + ((meaningText == null) ? 0 : meaningText.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gloss other = (Gloss) obj;
		if (languageCode != other.languageCode)
			return false;
		if (meaningText == null) {
			if (other.meaningText != null)
				return false;
		} else if (!meaningText.equals(other.meaningText))
			return false;
		return true;
	}
	
	
}
