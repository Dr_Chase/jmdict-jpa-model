package hu.unideb.jmdict.jmdict;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import hu.unideb.jmdict.jmdict.interfaces.Extractable;
import hu.unideb.jmdict.jmdict.interfaces.HasChildren;
import hu.unideb.jmdict.jmdict.interfaces.HasParent;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @NoArgsConstructor @ToString
public class Sense implements HasParent, HasChildren, Extractable {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "entry_id")
	private Entry entry;
	
	@Column(columnDefinition="TEXT") //for postgres
	private String text;
	
	@OneToMany( mappedBy = "sense", orphanRemoval = true )
	private List<Gloss> glosses = new ArrayList<>();

	@Override
	public void setParent(HasChildren parent) {
		
		if(  !(parent instanceof Entry) )	{
			
			throw new IllegalArgumentException("Input must be type of Entry");
		}
		setEntry((Entry) parent);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((glosses == null) ? 0 : glosses.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sense other = (Sense) obj;
		if (glosses == null) {
			if (other.glosses != null)
				return false;
		} else if (!glosses.equals(other.glosses))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}
	
	
}
