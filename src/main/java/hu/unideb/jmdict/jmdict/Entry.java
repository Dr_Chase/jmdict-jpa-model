package hu.unideb.jmdict.jmdict;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import hu.unideb.jmdict.jmdict.interfaces.Extractable;
import hu.unideb.jmdict.jmdict.interfaces.HasChildren;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @NoArgsConstructor @ToString
public class Entry implements HasChildren, Extractable{

	@Id
	@GeneratedValue
	private Long id;
	
	/*
	 * This field references the ent_seq inside entry.
	 */
	@Column(unique = true)
	private Long entrySequence;

	@OneToMany( mappedBy = "entry",  orphanRemoval = true )
	private List<KanjiElement> kanjiElements = new ArrayList<>();
	
	@OneToMany( mappedBy = "entry",  orphanRemoval = true )
	private List<ReadingElement> readingElements = new ArrayList<>();
	
	@OneToMany( mappedBy = "entry",  orphanRemoval = true )
	private List<Sense> senses = new ArrayList<>();

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entrySequence == null) ? 0 : entrySequence.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Entry other = (Entry) obj;
		if (entrySequence == null) {
			if (other.entrySequence != null)
				return false;
		} else if (!entrySequence.equals(other.entrySequence))
			return false;
		return true;
	}
	
	
	
}
