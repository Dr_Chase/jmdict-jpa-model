package hu.unideb.jmdict.jmdict;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import hu.unideb.jmdict.jmdict.interfaces.Extractable;
import hu.unideb.jmdict.jmdict.interfaces.HasChildren;
import hu.unideb.jmdict.jmdict.interfaces.HasParent;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter @Setter @NoArgsConstructor @ToString
public class ReadingElementPriority implements HasParent, HasChildren, Extractable {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@ManyToOne
	private ReadingElement readingElement;
	
	@Column(columnDefinition="TEXT") //for postgres
	private String priority;
	
	@Override
	public void setParent(HasChildren parent) {
		
		if(  !(parent instanceof ReadingElement) )	{
			
			throw new IllegalArgumentException("Input must be type of ReadingElement");
		}
		
		setReadingElement( (ReadingElement)parent);
	}
}
